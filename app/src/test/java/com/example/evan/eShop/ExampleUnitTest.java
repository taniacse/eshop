package com.example.evan.eShop;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <activity_men_t_shirt href="http://d.android.com/tools/testing">Testing documentation</activity_men_t_shirt>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }
}