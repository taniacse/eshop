package com.example.evan.eShop.model;

/**
 * Created by sbsatter on 3/24/17.
 */
public class ProductSizeColor {
    int id;
    Color color;
    Size size;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

}
