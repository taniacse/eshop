package com.example.evan.eShop.interfaces;

import com.example.evan.eShop.ImageModel;
import com.example.evan.eShop.model.Image;
import com.example.evan.eShop.model.ProductCommons;

import java.util.ArrayList;

import retrofit.http.Headers;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

/**
 * Created by EVAN on 5/2/2017.
 */

public interface IdWiseImage {
    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @GET("productImage/{id}")
    Call<ArrayList<Image>> getImage(@Header("deviceId") String deviceId, @Header("sessionId") String contentRange, @Path("id") int id);
}
