package com.example.evan.eShop.interfaces;

import com.example.evan.eShop.fd;
import com.example.evan.eShop.model.Category;
import com.example.evan.eShop.model.Product;
import com.example.evan.eShop.model.ProductCommons;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Headers;
import retrofit2.Call;

/**
 * Created by Opu on 4/7/2017.
 */

public interface ProductLoader {
    String url = "productCommons";
    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @GET("/list")
    public void Category(Callback<ArrayList<ProductCommons>> response);


}
