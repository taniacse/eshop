package com.example.evan.eShop.model.enums;

/**
 * Created by sbsatter on 3/21/17.
 */
public enum PaymentStatus {
    PAID, UNPAID
}
