package com.example.evan.eShop.sqliteDatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by EVAN on 4/6/2017.
 */

public class ProductDatabase extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 15;
    private static final String DATABASE_NAME = "contactsManager";
    private static final String TABLE_PRODUCTS = "products";
    public static final String KEY_ID = "id";
    private static final String KEY_Quantity = "quantity";
    private static final String KEY_NAME = "name";
    private static final String KEY_CODE = "code";
    private static final String KEY_CONDITION = "condition";
    private static final String KEY_DESCRIPTION = "description";
    private static final String KEY_IMAGE= "image";

    public ProductDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_PRODUCTS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_Quantity + " TEXT," + KEY_NAME + " TEXT," + KEY_CODE + " TEXT,"
                + KEY_CONDITION + " TEXT, " + KEY_DESCRIPTION + " TEXT," + KEY_IMAGE + " BLOB" +")";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCTS);
        onCreate(db);
    }

    public void addProduct(Product product) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ID, product.getId());
        values.put(KEY_NAME, product.getName());
        values.put(KEY_CODE, product.getCode());
        values.put(KEY_CONDITION, product.getCondition());
        values.put(KEY_DESCRIPTION, product.getDescription());
        values.put(KEY_Quantity, product.getQuantity());
        values.put(KEY_IMAGE, product.getData());
        db.insert(TABLE_PRODUCTS, null, values);
        db.close();
    }

    public List<Product> getAllProducts() {
        List<Product> contactList = new ArrayList<Product>();
        String selectQuery = "SELECT  * FROM " + TABLE_PRODUCTS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Product contact = new Product(Integer.parseInt(cursor.getString(0)), (Integer.parseInt(cursor.getString(1))), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getBlob(6));
                contactList.add(contact);
            } while (cursor.moveToNext());
        }

        return contactList;
    }

    public void updateContact(int id, int count) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_Quantity, count);
        Log.e("updateContact", KEY_Quantity + "========" + count);
        db.update(TABLE_PRODUCTS, values, "ID = ?", new String[]{String.valueOf(id)});
    }


    public void deleteContact(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_PRODUCTS, KEY_ID + " = ?",
                new String[]{String.valueOf(id)});
        db.close();
    }

    public boolean Exists(int _id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT id FROM " + TABLE_PRODUCTS + " WHERE " + KEY_ID + "=?",
                new String[]{String.valueOf(_id)});
        boolean exists = (cursor.getCount() > 0);
        cursor.close();
        return exists;
    }

    public Product getContact(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_PRODUCTS, new String[]{KEY_ID, KEY_Quantity,
                        KEY_NAME, KEY_CODE, KEY_CONDITION, KEY_DESCRIPTION,KEY_IMAGE}, KEY_ID + "=?",

                new String[]{String.valueOf(id)}, null, null, null);
        if (cursor != null)
            cursor.moveToNext();

        Product contact = new Product(Integer.parseInt(cursor.getString(0)), (Integer.parseInt(cursor.getString(1))), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5),cursor.getBlob(6));

        cursor.moveToNext();
        return contact;
    }


}