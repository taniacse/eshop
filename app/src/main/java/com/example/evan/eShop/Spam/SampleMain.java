package com.example.evan.eShop.Spam;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.evan.eShop.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

//import retrofit.Callback;
//import retrofit.RestAdapter;
//import retrofit.RetrofitError;
//import retrofit.client.Response;
//import retrofit.converter.GsonConverter;

public class SampleMain extends Fragment  {
    private RecyclerView recyclerView;
    private ArrayList<SampleAdapterModel> data;
    RecyclerView.LayoutManager layoutManager;
    private SampleAdapter adapter;
    private ProgressDialog progress;
    private int employeeId;
    SearchView sv;
    private Context mContext;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_sample_main, container, false);
        recyclerView = (RecyclerView) v.findViewById(R.id.card_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(), 2);
        recyclerView.setLayoutManager(layoutManager);
        getdata();
        return v;

    }

    private void getdata() {

        Gson gson = new GsonBuilder().create();
        progress = new ProgressDialog(getContext());
        progress.setMessage("Data Loading ... Please Wait...");
        progress.show();
        final RestAdapter adapter1 = new RestAdapter.Builder().setConverter(new GsonConverter(gson)).setEndpoint(SampleInterface.url).build();
////        final SampleInterface restInterface = adapter1.create(SampleInterface.class);
////        restInterface.Load(new Callback<ArrayList<SampleAdapterModel>>() {
////            @Override
////            public void success(ArrayList<SampleAdapterModel> jsonResponses, Response response) {
////                data = jsonResponses;
////
////                adapter = new SampleAdapter(mContext, data, SampleMain.this);
////                recyclerView.setAdapter(adapter);
////                progress.hide();
////            }
////
////            public void failure(RetrofitError error) {
////                Log.e("fg", "On Failure Exception:" + error);
////            }
////        });
//    }

//    @Override
//    public void ShowDetails(SampleAdapterModel officeEmployeeModel) {
//        ProductAddFragment fragment = new ProductAddFragment();
//        Bundle bundle = new Bundle();
//     //   bundle.putInt("id", officeEmployeeModel.getId());
//
//        bundle.putString("name", officeEmployeeModel.getProduct().getName());
//        bundle.putString("code", officeEmployeeModel.getProduct().getCode());
//        bundle.putString("Condition", officeEmployeeModel.getProduct().getCondition());
//        bundle.putString("Description", officeEmployeeModel.getProduct().getDescription());
//        //bundle.putDouble("UnitPrice", officeEmployeeModel.getProduct().getUnitPrice());
//
//
//        fragment.setArguments(bundle);
//        android.support.v4.app.FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction().addToBackStack(null);
//        fragmentTransaction.replace(R.id.app_bar_main, fragment);
//        fragmentTransaction.commit();
//    }
    }
}