package com.example.evan.eShop.model;

/**
 * Created by SayedMahmudRaihan on 3/9/2017.
 */
public class Role {
    private Integer id;
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
