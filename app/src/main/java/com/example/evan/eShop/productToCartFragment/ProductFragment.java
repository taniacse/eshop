package com.example.evan.eShop.productToCartFragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.evan.eShop.CommonData;
import com.example.evan.eShop.R;
import com.example.evan.eShop.adapter.ImageAdapter;
import com.example.evan.eShop.adapter.ProductAdapter;
import com.example.evan.eShop.apiClient.CategoryApiClient;
import com.example.evan.eShop.apiClient.ImageApiClient;
import com.example.evan.eShop.interfaces.IdWiseImage;
import com.example.evan.eShop.interfaces.IdWiseProduct;
import com.example.evan.eShop.interfaces.ProductListener;
import com.example.evan.eShop.interfaces.ProductLoader;
import com.example.evan.eShop.main.MainActivity;
import com.example.evan.eShop.model.Image;
import com.example.evan.eShop.model.Product;
import com.example.evan.eShop.model.ProductCommons;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Callback;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit2.Call;

public class ProductFragment extends Fragment implements ProductListener {
    private RecyclerView recyclerView;
    private ArrayList<Product> productArrayList;
    private ArrayList<Image> ImageArrayLists;
    private Context context;
    private ProductAdapter productAdapter;
    private ImageAdapter ImageAdapter;
    int id = 1;
    ProgressDialog progress;
    private String baseUrl;
    private String sessionId, deviceId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product, container, false);



        CommonData commonData = (CommonData) getActivity().getApplication();
        baseUrl = commonData.getBaseUrl();
        recyclerView = (RecyclerView) view.findViewById(R.id.card_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(), 2);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        MainActivity activitys = (MainActivity) getActivity();
        sessionId = activitys.getData();
        deviceId = activitys.getDeviceData();
        //getAllData();

        //getProductInfos();
        getProductInfo();
        return view;
    }
    /*---------------- Get JSON Data --------------------- */




    private void getProductInfo() {
        progress = new ProgressDialog(getContext());
        progress.setMessage("Data Loading ... Please Wait...");
        progress.show();
        IdWiseProduct apiService =
                CategoryApiClient.getClient().create(IdWiseProduct.class);
        Call<ArrayList<Product>> call = apiService.getTopRatedMovies(deviceId, sessionId, id);
        call.enqueue(new Callback<ArrayList<Product>>() {
            @Override
            public void onResponse(Call<ArrayList<Product>> call, retrofit2.Response<ArrayList<Product>> response) {
                if (response.isSuccessful()) {
                    productArrayList = response.body();
                    productAdapter = new ProductAdapter(context, productArrayList, ProductFragment.this,deviceId,sessionId);
                    recyclerView.setAdapter(productAdapter);
                    progress.dismiss();
                }
            }
            @Override
            public void onFailure(Call<ArrayList<Product>> call, Throwable t) {
                Log.e("$%xx$%$", "e" + t.getMessage());
            }
        });
//
    }
    //    private void getAllData() {
//        progress = new ProgressDialog(getContext());
//        progress.setMessage("Data Loading ... Please Wait...");
//        progress.show();
//        RequestInterceptor requestInterceptor = new RequestInterceptor() {
    //@Override
//            public void intercept(RequestFacade request) {
//                request.addHeader("sessionId", sessionId);
//                request.addHeader("deviceId", deviceId);
//            }
//        };
//        final RestAdapter adapter1 = new RestAdapter.Builder().setEndpoint(baseUrl + ProductLoader.url).setRequestInterceptor(requestInterceptor).build();
//        final ProductLoader restInterface = adapter1.create(ProductLoader.class);
//        restInterface.Category(new Callback<ArrayList<ProductCommons>>() {
//            @Override
//            public void success(ArrayList<ProductCommons> products, Response response) {
//                productArrayList = products;
//                productAdapter = new ProductAdapter(context, productArrayList, ProductFragment.this);
//                recyclerView.setAdapter(productAdapter);
//                progress.dismiss();
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                Log.e("****", "D" + error.getMessage());
//            }
//        });
//    }
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
    @Override
    public void showProductDetails(Product product) {
        ProductAddFragment fragment = new ProductAddFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("id", product.getProductCommons().getId());
        bundle.putString("name", product.getProductCommons().getName());
        bundle.putString("code", product.getProductCommons().getCode());
        bundle.putString("Condition", product.getProductCommons().getCondition());
        bundle.putString("Description", product.getProductCommons().getDescription());
        bundle.putDouble("unitPrice", product.getProductCommons().getUnitPrice());
        // bundle.putByteArray("Image",image.getData());
        fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction().addToBackStack(null);
        fragmentTransaction.replace(R.id.app_bar_main, fragment);
        fragmentTransaction.commit();
    }
}
