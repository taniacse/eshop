package com.example.evan.eShop.interfaces;

import com.example.evan.eShop.model.Product;
import com.example.evan.eShop.model.ProductCommons;
import com.google.gson.annotations.SerializedName;


//
import java.util.ArrayList;



import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Path;


/**
 * Created by EVAN on 4/11/2017.
 */

public interface IdWiseProduct {
    @Headers({ "Content-Type: application/json;charset=UTF-8"})

    @GET("cat/{id}")


    Call<ArrayList<Product>> getTopRatedMovies( @Header("deviceId") String deviceId,@Header("sessionId") String contentRange,@Path("id") int id);

}
