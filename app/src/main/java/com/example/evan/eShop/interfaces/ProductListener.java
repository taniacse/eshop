package com.example.evan.eShop.interfaces;

import com.example.evan.eShop.model.Image;
import com.example.evan.eShop.model.Product;
import com.example.evan.eShop.model.ProductCommons;

/**
 * Created by Opu on 4/8/2017.
 */

public interface ProductListener {
    void showProductDetails(Product product);


    // void showProductDetails(ProductCommons product);
}
