package com.example.evan.eShop.interfaces;

import com.example.evan.eShop.model.*;
import com.example.evan.eShop.model.Product;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by EVAN on 4/13/2017.
 */

public interface ProductItem {
    @POST("add")
    Call<Cart> FooResponse (@Header("deviceId") String deviceId,@Header("sessionId") String contentRange,@Body Cart body);
}
