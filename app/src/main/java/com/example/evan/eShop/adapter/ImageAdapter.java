package com.example.evan.eShop.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.evan.eShop.R;
import com.example.evan.eShop.model.Image;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;

/**
 * Created by EVAN on 5/7/2017.
 */

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ViewHolder> {
    public ArrayList<Image> android;

    Context context;

    public ImageAdapter(Context context, ArrayList<Image> android) {
        this.context = context;
        this.android = android;

    }

    @Override
    public ImageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.testadapter, parent, false);
        return new ImageAdapter.ViewHolder(view);
    }

    public void onBindViewHolder(ImageAdapter.ViewHolder holder, final int i) {

        byte[] images = android.get(i).getData();
        Bitmap bitmap = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        bitmap = BitmapFactory.decodeByteArray(images, 0, images.length, options); //Convert bytearray to bitmap
        //for performance free the memmory allocated by the bytearray and the blob variable
        if (bitmap != null) {
            holder.a.setImageBitmap(bitmap);
        } else {
//            System.out.println("bitmap is null");
        }


    }

    @Override
    public int getItemCount() {
        return android.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name, tv_version, tv_api_level, t;
        private ImageView a;

        public ViewHolder(View view) {
            super(view);

            a = (ImageView) view.findViewById(R.id.image);
        }
    }

}
