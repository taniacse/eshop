package com.example.evan.eShop.productToCartFragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.evan.eShop.ImageModel;
import com.example.evan.eShop.R;
import com.example.evan.eShop.SlidingImage_Adapter;
import com.example.evan.eShop.adapter.ProductAdapter;
import com.example.evan.eShop.model.Image;
import com.example.evan.eShop.sqliteDatabase.Product;
import com.example.evan.eShop.sqliteDatabase.ProductDatabase;
import com.google.gson.Gson;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;


public class ProductAddFragment extends Fragment {
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private ArrayList<Image> imageModelArrayList;
  ProductAdapter productAdapter;
    private int[] myImageList = new int[]{R.drawable.aa, R.drawable.header,
            R.drawable.men,R.drawable.banner3
            ,R.drawable.images,R.drawable.evannn};
    TextView t1, itemAmount, t3, removeItem, addItem,sample;
    int test1;
    String test2;
    String test3;
    String test4;
    String test5;
    private String productName, productDescription;
    private Double productPrice;
    Button b;
    public ProductDatabase dbAdapter;
    int count = 0;
    byte [] imagesByte,as;
    String imagesBytes;

    private Map<Integer, Integer> mCardItemMap;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_test, container, false);
       imageModelArrayList =    ProductAdapter.images;


        imagesByte=ProductAdapter.images.get(0).getData();


//       imagesByte = imagesBytes.getBytes();
  //as = new byte[imagesBytes.length()];




//        Bitmap bitmap = null;
//        BitmapFactory.Options options = new BitmapFactory.Options();
//        bitmap = BitmapFactory.decodeByteArray(imagesByte, 0, imagesByte.length, options); //Convert bytearray to bitmap
//        //for performance free the memmory allocated by the bytearray and the blob variable
//        if (bitmap != null) {
//            viewHolder.a.setImageBitmap(bitmap);
//        } else {
////            System.out.println("bitmap is null");
//        }
        Bundle bundle = this.getArguments();
        mPager = (ViewPager) view.findViewById(R.id.pager);
        CirclePageIndicator indicator = (CirclePageIndicator)
                view.findViewById(R.id.indicator);
        mPager.setAdapter(new SlidingImage_Adapter(getActivity(),imageModelArrayList));
        indicator.setViewPager(mPager);
        final float density = getResources().getDisplayMetrics().density;
        indicator.setRadius(5 * density);
        NUM_PAGES =imageModelArrayList.size();
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });


        init();
        // int Latitude = bundle.getInt("name", 0);
        dbAdapter = new ProductDatabase(getActivity());
        test1 = bundle.getInt("id", 0);
        test2 = bundle.getString("name", null);
        test3 = bundle.getString("code", null);
        test4 = bundle.getString("Condition", null);
        test5 = bundle.getString("Description", null);
        //  test6 = bundle.getDouble("UnitPrice", 0);

        productName = bundle.getString("name", null);
        productPrice = bundle.getDouble("unitPrice", 0);
        productDescription = bundle.getString("Description", null);

        Log.e("", productName + " " + productPrice + " " + productDescription);

        Log.e("_________", "Product ID :" + productPrice);
        Log.e("_________", "Product Name: " + test2);
        Log.e("Product Code", " " + test1);

        t1 = (TextView) view.findViewById(R.id.product_price);
        itemAmount = (TextView) view.findViewById(R.id.iteam_amount);
        t3 = (TextView) view.findViewById(R.id.product_description);
        removeItem = (TextView) view.findViewById(R.id.remove_item);
        addItem = (TextView) view.findViewById(R.id.add_item);
        sample = (TextView) view.findViewById(R.id.tests);
        b = (Button) view.findViewById(R.id.bag);

        t1.setText("Price :" + productPrice.toString());
        //itemAmount.setText(productDiscount);
        t3.setText(productDescription);
//        Log.d("FSDFSD","Sdfs"+mCartItems.get(1).getQuantity());
        if (dbAdapter.Exists(test1)) {
            //b.setEnabled(false);
            b.setVisibility(View.GONE);
            itemAmount.setVisibility(View.VISIBLE);
            removeItem.setVisibility(View.VISIBLE);
            addItem.setVisibility(View.VISIBLE);
//            ArrayList<Product> contacts = (ArrayList<Product>) dbAdapter.getContact(test1);
            Product contact = dbAdapter.getContact(test1);

            // Log.e("++++++","____"+contact.getId());
            itemAmount.setText(String.valueOf(contact.getQuantity()));


            addItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //itemAmount.setText(String.valueOf(mCartItems.get(2).getQuantity()));
                    count = Integer.parseInt(itemAmount.getText().toString());
                    count++;
                    itemAmount.setText(String.valueOf(count));

                    onClickUpdate(test1, count);
                }
            });

            removeItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    count = Integer.parseInt(itemAmount.getText().toString());
                    count--;
                    itemAmount.setText(String.valueOf(count));
                    onClickUpdate(test1, count);

                }
            });
            //addItem.setBackgroundColor(Color.rgb(105,105,105));
        } else {
            b.setVisibility(View.VISIBLE);
            itemAmount.setVisibility(View.GONE);
            removeItem.setVisibility(View.GONE);
            addItem.setVisibility(View.GONE);
//            sample.setText(++count);
            b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("Insert: ", "Inserting ..");
                    Toast.makeText(getActivity(), "SuccessFully Add To the Cart", Toast.LENGTH_LONG).show();
                    dbAdapter.addProduct(new Product(test1, 1, test2, test3, test4, test5,imagesByte));



                }
            });


        }


        return view;
    }


    public interface CountUpdate {
        void onCountUpdate(int productId, int count);
    }

    public interface CountDelete {
        void onCountDelete(int productId);
    }

    public void onCountUpdate(int productId, int count) {
        Log.e("onCountUpdate", "= =========="+count);
        //if(mCardItemMap.)
        mCardItemMap.put(productId, count);
    }




    public void onClickUpdate(int id, int count) {

        dbAdapter.updateContact(id, count);
    }
    private ArrayList<ImageModel> populateList(){

        ArrayList<ImageModel> list = new ArrayList<>();

        for(int i = 0; i < 6; i++){
            ImageModel imageModel = new ImageModel();
            imageModel.setImage_drawable(myImageList[i]);
            list.add(imageModel);
        }

        return list;
    }

    private void init() {



    }

}
