package com.example.evan.eShop.main;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.evan.eShop.R;
import com.example.evan.eShop.accountDetails.AccountDetailsMain;
import com.example.evan.eShop.dashBoardFragment.KidsFragment;
import com.example.evan.eShop.dashBoardFragment.MenFragment;
import com.example.evan.eShop.dashBoardFragment.WomenFragment;
import com.example.evan.eShop.dashboard.HomeFragment;
import com.example.evan.eShop.productToCartFragment.CartFragment;
import com.example.evan.eShop.profile.ProfilleMain;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private String baseUrl;
    String name;
    String id;
    String loginame;
    TextView username;
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        //View tollView = toolbar.getChildAt(0);
        username = (TextView) headerView.findViewById(R.id.Emp_username);
//        CommonData commonData = (CommonData) getApplication();
//        baseUrl = commonData.getBaseUrl();
        Intent intent = getIntent();
        name = intent.getStringExtra("sessionId");
        id = intent.getStringExtra("deviceId");
        loginame = intent.getStringExtra("name");
        username.setText(loginame);
        Log.d("deviceId", "SD" + id);
        Log.d("loginame", "SD" + loginame);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        com.example.evan.eShop.dashboard.HomeFragment homeFragment = new HomeFragment();
        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.app_bar_main, homeFragment);
        fragmentTransaction.commit();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            return;
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem item1 = menu.findItem(R.id.action_alarm);
        MenuItemCompat.setActionView(item1, R.layout.notification_update_count_layout);
        final View menu_hotlist = menu.findItem(R.id.action_alarm).getActionView();
        Button textViews = (Button) menu_hotlist.findViewById(R.id.notification);
        textViews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.app_bar_main, new CartFragment()).addToBackStack(null).commit();
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify activity_men_t_shirt parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.user) {
////            SampleMain dashboard=new SampleMain();
////            android.support.v4.app.FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
////            fragmentTransaction.replace(R.id.app_bar_main, dashboard).addToBackStack(null);
////            fragmentTransaction.commit();
////            return true;
////
////            Intent i = new Intent(this,LoginSignUpActivity.class);
////            startActivity(i);
////            ProductFragment productFragment = new ProductFragment();
////            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
////            fragmentTransaction.replace(R.id.app_bar_main,productFragment).addToBackStack(null).commit();
////            fragmentTransaction.commit();
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.men) {
            MenFragment dashboard = new MenFragment();
            android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.app_bar_main, dashboard).addToBackStack(null).commit();
        } else if (id == R.id.Women) {
            WomenFragment dashboard = new WomenFragment();
            android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.app_bar_main, dashboard).addToBackStack(null).commit();
        } else if (id == R.id.nav_dashboard) {
            HomeFragment homeFragment = new HomeFragment();
            android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.app_bar_main, homeFragment).addToBackStack(null).commit();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        // .... other stuff in my onResume ....
        this.doubleBackToExitPressedOnce = false;
    }


    public String getData() {
        return name;
    }

    public String getDeviceData() {
        return id;
    }
}
