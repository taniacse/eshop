package com.example.evan.eShop.Login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.evan.eShop.CommonData;
import com.example.evan.eShop.R;
import com.example.evan.eShop.interfaces.LoginInterface;
import com.example.evan.eShop.main.MainActivity;
import com.example.evan.eShop.model.UserCredentials;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;


public class Login extends AppCompatActivity {

    TextView etUserName;
    EditText etPassword;
    Button btnLogin;
    private String savedName;
    private String savedPass;
    private ProgressDialog progress;
    private String baseUrl;
    private UserCredentials userCredentials;
    public static final String PREFS_NAME = "PingBusPrefs";
    public static final String PREFS_SEARCH_HISTORY = "SearchHistory";
    private SharedPreferences settings;
    private Set<String> history;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        CommonData commonData = (CommonData) getApplication();
        baseUrl = commonData.getBaseUrl();
        final String id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.d("deviceid", "Xvcx" + id);
       etUserName = (TextView) findViewById(R.id.username);

        etPassword = (EditText) findViewById(R.id.password);
        btnLogin = (Button) findViewById(R.id.login);
        if (savedInstanceState != null) {
            savedInstanceState.get(savedName);
            etUserName.setText(savedName);
            savedInstanceState.get(savedPass);
            etPassword.setText(savedPass);
        } else {
            btnLogin.setOnClickListener(new View.OnClickListener() {


                @Override
                public void onClick(View v) {

                    if (CheckFieldValidation()) {
                        progress = new ProgressDialog(Login.this);
                        progress.setMessage("Authentication Checking ... Please Wait...");
                        progress.show();
                        Gson gson = new GsonBuilder().create();

                        RequestInterceptor requestInterceptor = new RequestInterceptor() {
                            @Override
                            public void intercept(RequestFacade request) {

                                request.addHeader("deviceId", id);
                            }
                        };
                        final

                        RestAdapter adapter = new RestAdapter.Builder().setConverter(new GsonConverter(gson)).setEndpoint(baseUrl + LoginInterface.url).setRequestInterceptor(requestInterceptor).build();
                        final LoginInterface restInterface = adapter.create(LoginInterface.class);
                        String username = etUserName.getText().toString().trim();
                        String password = etPassword.getText().toString().trim();
                        restInterface.Login(new UserCredentials(username, password), new Callback<HashMap<String, String>>() {

                            @Override
                            public void success(HashMap<String, String> stringObjectHashMap, Response response) {


                                String s = stringObjectHashMap.put("sessionId", etUserName.getText().toString());
                                String loginname = stringObjectHashMap.put("name", etUserName.getText().toString());
                                boolean s1 = Boolean.parseBoolean(stringObjectHashMap.put("isAuthenticated", etUserName.getText().toString()));

                                if (s1 == true) {

                                    etUserName.setText("");
                                    etPassword.setText("");
                                    Toast.makeText(Login.this, "Login SuccessFully", Toast.LENGTH_SHORT).show();
                                    saveInSharedPrefImages(s);
                                    saveInSharedPrefDevice(id);
                                    saveInSharedPrefLoginName(loginname);
                                    Intent i = new Intent(Login.this, MainActivity.class);
                                    i.putExtra("sessionId", getSharedPrefDataImage());
                                    i.putExtra("deviceId", getSharedPrefDataDevice());
                                    i.putExtra("name", getSharedPrefLoginName());
                                    progress.dismiss();
                                    startActivity(i);
                                } else {
                                    Toast.makeText(Login.this, " Wrong User", Toast.LENGTH_LONG).show();

                                    progress.dismiss();
                                }
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                Log.d("evanS", "Log 4: " + error.toString());
                                startActivity(getIntent());
                                String merror = error.getMessage();
                                Toast.makeText(Login.this, merror, Toast.LENGTH_LONG).show();
                            }

                        });
                    }

                }
            });
        }

    }



    private boolean CheckFieldValidation() {

        boolean valid = true;

        if (etUserName.getText().toString().equals("")) {
            etUserName.setError("Can't be Empty");
            valid = false;

        } else if (etPassword.getText().toString().equals("")) {
            etPassword.setError("Can't be Empty");
            valid = false;
        } else if (etUserName.length() < 5) {
            etUserName.setError("at least 5 characters long");
            valid = false;
        } else if (etPassword.length() < 5) {
            etPassword.setError("at least 5 characters long");
            valid = false;
        }


        return valid;
    }

    private void saveInSharedPrefImages(String r) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("sessionId", r);
        editor.commit();
    }

    public String getSharedPrefDataImage() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String companyId = sharedPref.getString("sessionId", null);
        return companyId;
    }

    private void saveInSharedPrefDevice(String r) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("deviceId", r);
        editor.commit();
    }

    public String getSharedPrefDataDevice() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String companyId = sharedPref.getString("deviceId", null);
        return companyId;
    }

    private void saveInSharedPrefLoginName(String r) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("name", r);
        editor.commit();
    }

    public String getSharedPrefLoginName() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String companyId = sharedPref.getString("name", null);
        return companyId;
    }
}