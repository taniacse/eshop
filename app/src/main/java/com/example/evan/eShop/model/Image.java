package com.example.evan.eShop.model;



import com.example.evan.eShop.model.enums.FileTypeEnum;

import java.util.Date;

/**
 * Created by SayedMahmudRaihan on 3/9/2017.
 */
public class Image {
    private Integer id;
    private byte[] data;
    private String imgHash;
    private String fileName;
    private Integer fileSize;
    private Date createdAt;
    private FileTypeEnum fileType;


    public Image(byte[] data, FileTypeEnum fileType) {
       // this(data, MessageDigestUtil.getDigest("md5", data), fileType);
    }

    public Image() {
    }

    public Image(byte[] data, String fileName, FileTypeEnum fileType) {
        this.fileType = fileType;
        this.data = data;
        this.fileName = fileName + ".png";
        if (this.data != null && this.data.length > 0) {
            this.fileSize = this.data.length;
            //this.imgHash = MessageDigestUtil.getDigest("sha-1", this.data);
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public String getImgHash() {
        return imgHash;
    }

    public void setImgHash(String imgHash) {
        this.imgHash = imgHash;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Integer getFileSize() {
        return fileSize;
    }

    public void setFileSize(Integer fileSize) {
        this.fileSize = fileSize;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public FileTypeEnum getFileType() {
        return fileType;
    }

    public void setFileType(FileTypeEnum fileType) {
        this.fileType = fileType;
    }
}
