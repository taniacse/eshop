package com.example.evan.eShop.apiClient;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by EVAN on 5/2/2017.
 */

public class ImageApiClient {
    public static final String BASE_URL = "http://192.168.1.244:8080/eshopservice/image/";
    private static Retrofit retrofit = null;


    public static Retrofit getClient() {

        if (retrofit==null) {

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
