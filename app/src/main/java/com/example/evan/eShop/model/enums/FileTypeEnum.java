package com.example.evan.eShop.model.enums;

/**
 * Created by SayedMahmudRaihan on 3/13/2017.
 */
public enum FileTypeEnum {
    IMAGE, LOGO, MERCHANT, MESSAGE, TEXT, REWARD
}
