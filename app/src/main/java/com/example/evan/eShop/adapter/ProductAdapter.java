package com.example.evan.eShop.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.evan.eShop.ImageModel;
import com.example.evan.eShop.R;
import com.example.evan.eShop.apiClient.ImageApiClient;
import com.example.evan.eShop.interfaces.IdWiseImage;
import com.example.evan.eShop.interfaces.ProductListener;
import com.example.evan.eShop.model.Image;
import com.example.evan.eShop.model.Product;
import com.example.evan.eShop.productToCartFragment.ProductAddFragment;
import com.example.evan.eShop.productToCartFragment.ProductFragment;
import com.google.gson.Gson;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by Opu on 4/7/2017.
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {
    public ArrayList<Product> product;
    public static  ArrayList<Image> images;
    private ProductListener listener;
    private Context context;
    private String deviceId;
    private String sessionId;


    public ProductAdapter(Context mContext, ArrayList<Product> product, ProductListener listener, String deviceId, String sessionId) {
        this.context = mContext;
        this.product = product;
     //  /;/'' this.image = image;
        this.listener = listener;
        this.deviceId = deviceId;
        this.sessionId = sessionId;
    }

    @Override
    public ProductAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_jeans, parent, false);

        return new ProductAdapter.ViewHolder(view);
    }

    public void onBindViewHolder(ProductAdapter.ViewHolder holder, final int i) {
        holder.tv_name.setText(product.get(i).getProductCommons().getName());
        holder.tv_version.setText(product.get(i).getProductCommons().getDescription());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            listener.showProductDetails(product.get(i));
            }
        });
        getProductInfos(holder, product.get(i).getProductCommons().getId());



    }

    @Override
    public int getItemCount() {
        return product.size();

    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name, tv_version, tv_api_level, t;
        private ImageView a;

        public ViewHolder(View view) {
            super(view);
            tv_name = (TextView) view.findViewById(R.id.name);
            tv_version = (TextView) view.findViewById(R.id.condition);
            a = (ImageView) view.findViewById(R.id.image);
        }
    }

    public static Bitmap stringToBitmap(String fileString) {

        byte[] valueDecoded = Base64.decode(fileString, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(valueDecoded, 0,
                valueDecoded.length);
    }

    private boolean IsBase64Encoded(String value) {
        try {
            byte[] decodedString = Base64.decode(value, Base64.DEFAULT);
            BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void animate(RecyclerView.ViewHolder viewHolder) {
        final Animation animAnticipateOvershoot = AnimationUtils.loadAnimation(context, R.anim.bounce_interpolator);
        viewHolder.itemView.setAnimation(animAnticipateOvershoot);
    }

    public Bitmap ByteArrayToBitmap(byte[] byteArray) {
        ByteArrayInputStream arrayInputStream = new ByteArrayInputStream(byteArray);
        Bitmap bitmap = BitmapFactory.decodeStream(arrayInputStream);
        return bitmap;
    }

    private void getProductInfos(final ProductAdapter.ViewHolder viewHolder, int id) {

        IdWiseImage apiService =
                ImageApiClient.getClient().create(IdWiseImage.class);
        Call<ArrayList<Image>> call = apiService.getImage(deviceId, sessionId, id);
        call.enqueue(new Callback<ArrayList<Image>>() {
            @Override
            public void onResponse(Call<ArrayList<Image>> call, retrofit2.Response<ArrayList<Image>> response) {
                if (response.isSuccessful()) {
                    images= response.body();

                    Log.e("Success", new Gson().toJson(images));

                    byte[] imagesByte = response.body().get(0).getData();

                    Log.e("fARHAD","///"+imagesByte);

                    Bitmap bitmap = null;
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    bitmap = BitmapFactory.decodeByteArray(imagesByte, 0, imagesByte.length, options); //Convert bytearray to bitmap
                    //for performance free the memmory allocated by the bytearray and the blob variable
                    if (bitmap != null) {
                        viewHolder.a.setImageBitmap(bitmap);
                    } else {
//            System.out.println("bitmap is null");
                    }

                }
            }

            @Override
            public void onFailure(Call<ArrayList<Image>> call, Throwable t) {
                Log.e("$%xx$%$", "e" + t.getMessage());
            }
        });
//
    }


}
