package com.example.evan.eShop.model;

/**
 * Created by sbsatter on 3/24/17.
 */
public class Size {
    private int id;
    private String name;
    private String dimension;

    public Size(int size_id, String size, String dimension) {
        this.id = size_id;
        this.name = size;
        this.dimension = dimension;
    }

    public Size() {

    }

    public String getDimension() {
        return dimension;
    }

    public void setDimension(String dimension) {
        this.dimension = dimension;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
