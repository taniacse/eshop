package com.example.evan.eShop.dashboard;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.evan.eShop.R;
import com.example.evan.eShop.dashBoardFragment.KidsFragment;
import com.example.evan.eShop.dashBoardFragment.MenFragment;
import com.example.evan.eShop.dashBoardFragment.WomenFragment;


public class HomeFragment extends Fragment {

    private RecyclerView recyclerView;
    ImageView imageView,imageView1,imageView2;
    private  RecyclerView.LayoutManager layoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v =inflater.inflate(R.layout.fragment_home, container, false);
        imageView = (ImageView) v.findViewById(R.id.image);
        TextView tv1 = (TextView) v.findViewById(R.id.TextView03);
        tv1.setSelected(true);  //
        TextView tv = (TextView)v.findViewById(R.id.tren);
        tv.setSelected(true);
//        tv.setEllipsize(TextUtils.TruncateAt.MARQUEE);
//        tv.setSingleLine(true);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MenFragment dashboard=new MenFragment();
                android.support.v4.app.FragmentTransaction fragmentTransaction=getFragmentManager().beginTransaction().addToBackStack(null);
                fragmentTransaction.replace(R.id.app_bar_main, dashboard);
                fragmentTransaction.commit();
        }
        });
               imageView1 = (ImageView) v.findViewById(R.id.sample);

        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WomenFragment dashboard=new WomenFragment();
                android.support.v4.app.FragmentTransaction fragmentTransaction=getFragmentManager().beginTransaction().addToBackStack(null);
                fragmentTransaction.replace(R.id.app_bar_main, dashboard);
                fragmentTransaction.commit();
            }
        });
        imageView2 = (ImageView) v.findViewById(R.id.samples);

        imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                KidsFragment dashboard=new KidsFragment();
                android.support.v4.app.FragmentTransaction fragmentTransaction=getFragmentManager().beginTransaction().addToBackStack(null);
                fragmentTransaction.replace(R.id.app_bar_main, dashboard);
                fragmentTransaction.commit();
            }
        });

//        recyclerView = (RecyclerView) v.findViewById(R.id.latest1);
//        recyclerView.setHasFixedSize(true);
//        layoutManager=new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false);
//        recyclerView.setLayoutManager(layoutManager);



        return v;


    }


}
