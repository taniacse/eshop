package com.example.evan.eShop.model;

/**
 * Created by sbsatter on 3/18/17.
 */
public class Category {
    private Integer id;
    private String name;
    private String categoryFor;
    private Integer parentCategoryId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategoryFor() {
        return categoryFor;
    }

    public void setCategoryFor(String categoryFor) {
        this.categoryFor = categoryFor;
    }

    public Integer getParentCategoryId() {
        return parentCategoryId;
    }

    public void setParentCategoryId(Integer parentCategoryId) {
        this.parentCategoryId = parentCategoryId;
    }
}
