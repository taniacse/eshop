package com.example.evan.eShop.model;

/**
 * Created by sbsatter on 3/14/17.
 */
public class UserCredentials {
    private Integer id;

    public UserSession getUserSession() {
        return userSession;
    }

    public void setUserSession(UserSession userSession) {
        this.userSession = userSession;
    }

    private String username;
    private String password;
    private boolean enabled;
    private Role role;
    private UserSession userSession;

    public UserCredentials(String username, String password ) {
        this.username = username;
        this.password = password;

    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public Boolean setEnabled(boolean enabled) {
        this.enabled = enabled;
        return null;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
