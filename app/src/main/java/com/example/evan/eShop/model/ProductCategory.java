package com.example.evan.eShop.model;

/**
 * Created by SayedMahmudRaihan on 3/9/2017.
 */
public class ProductCategory {
    private Integer id;
    private ProductCommons productCommons;
    private Category category;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ProductCommons getProductCommons() {
        return productCommons;
    }

    public void setProductCommons(ProductCommons productCommons) {
        this.productCommons = productCommons;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
