package com.example.evan.eShop.model;

import java.util.List;

/**
 * Created by sbsatter on 3/25/17.
 */
public class Product {
    private Integer id;
    private ProductCommons productCommons;
    private ProductSizeColor productSizeColors;
    private Integer unitsInStocks;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ProductCommons getProductCommons() {
        return productCommons;
    }

    public void setProductCommons(ProductCommons productCommons) {
        this.productCommons = productCommons;
    }

    public ProductSizeColor getProductSizeColors() {
        return productSizeColors;
    }

    public void setProductSizeColors(ProductSizeColor productSizeColors) {
        this.productSizeColors = productSizeColors;
    }

    public Integer getUnitsInStocks() {
        return unitsInStocks;
    }

    public void setUnitsInStocks(Integer unitsInStocks) {
        this.unitsInStocks = unitsInStocks;
    }
}
