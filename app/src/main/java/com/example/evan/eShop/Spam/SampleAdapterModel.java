package com.example.evan.eShop.Spam;


import com.example.evan.eShop.model.ProductCommons;
import com.example.evan.eShop.model.ProductSizeColor;
import com.google.gson.annotations.SerializedName;

/**
 * Created by EVAN on 3/21/2017.
 */

public class SampleAdapterModel {

    @SerializedName("productCommons")

    private ProductCommons product;

    public ProductSizeColor getProductSizeColor() {
        return productSizeColor;
    }

    public void setProductSizeColor(ProductSizeColor productSizeColor) {
        this.productSizeColor = productSizeColor;
    }

    @SerializedName("productSizeColors")

    private ProductSizeColor productSizeColor;
    private String condition;
    private int id;


    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }


    public String getCondition() {
        return condition;
    }
    public void setCondition(String condition) {
        this.condition = condition;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    private String description;
    public ProductCommons getProduct() {
        return product;
    }

    public void setProduct(ProductCommons product) {
        this.product = product;
    }

}
