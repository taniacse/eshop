package com.example.evan.eShop.productToCartFragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.evan.eShop.apiClient.CartApiClient;
import com.example.evan.eShop.R;
import com.example.evan.eShop.main.MainActivity;
import com.example.evan.eShop.sqliteDatabase.ProductDatabase;
import com.example.evan.eShop.sqliteDatabase.Product;
import com.example.evan.eShop.adapter.ProductAddAdapter;
import com.example.evan.eShop.interfaces.ProductItem;
import com.example.evan.eShop.model.Cart;
import com.example.evan.eShop.model.CartItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CartFragment extends Fragment implements ProductAddAdapter.CountUpdate, ProductAddAdapter.CountDelete {
    public ProductDatabase dbAdapter;
    ProductAddAdapter testAdapter;
    RecyclerView recyclerView;
    ProgressDialog progress;
    TextView Add;
    //  Product product;
    com.example.evan.eShop.model.Product product;
    private Map<Integer, Integer> mCardItemMap;

    private String sessionId;
    private String deviceId;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        dbAdapter = new ProductDatabase(getActivity());
        final View v = inflater.inflate(R.layout.fragment_blank2, container, false);
        MainActivity activitys = (MainActivity) getActivity();
        sessionId = activitys.getData();
        deviceId = activitys.getDeviceData();
        Log.e("deviceIdEvan", "VCVC" + deviceId);
        Log.e("sessionIdKhan", "VCVC" + sessionId);
        recyclerView = (RecyclerView) v.findViewById(R.id.card);

        Add = (TextView) v.findViewById(R.id.AddToCart);
        Add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<Product> contacts = (ArrayList<Product>) dbAdapter.getAllProducts();
                int count = 0;
                for (Product pro : contacts) {
                    count++;
                    Cart cart = new Cart();
                    cart.setConfirmed(false);
                    cart.setTotalPrice(1000);
                    HashMap<Integer, CartItem> cartItemMap = new HashMap<Integer, CartItem>();
                    CartItem cartItem = new CartItem();
                    cartItem.setQuantity(pro.getQuantity());
                    cartItem.setPrice(pro.getUnitPrice());
                    HashMap<String, Integer> hashMap = new HashMap<String, Integer>();
                    hashMap.put("id", pro.getId());
                    cartItem.setProduct(hashMap);
                    cartItemMap.put(pro.getId(), cartItem);
                    cart.setCartItems(cartItemMap);
                    getProductInfo(cart);
                    Toast.makeText(getContext(), String.valueOf(count), Toast.LENGTH_SHORT).show();
                }


            }
        });

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        mCardItemMap = new HashMap<>();
        Log.d("Reading: ", "Reading all contacts..");
        ArrayList<Product> contacts = (ArrayList<Product>) dbAdapter.getAllProducts();
        progress = new ProgressDialog(getActivity());
        progress.setMessage("Data Loading ... Please Wait...");
        progress.show();

        testAdapter = new ProductAddAdapter(getContext(), this, this, contacts);
        recyclerView.setAdapter(testAdapter);
        progress.dismiss();
        for (Product cn : contacts) {
            String log = "Quantity: " + cn.getData() +"Id: " + cn.getId() + " ,Name: " + cn.getName() + " ,Phone: " + cn.getCondition();
            Log.d("Name: ", log);
        }

        return v;
    }


    @Override
    public void onCountUpdate(int productId, int count) {
        Log.e("onCountUpdate", "= ==========" + count);
        mCardItemMap.put(productId, count);
    }


    @Override
    public void onPause() {
        for (Map.Entry<Integer, Integer> entry : mCardItemMap.entrySet()) {
            int key = entry.getKey();
            int value = entry.getValue();
            dbAdapter.updateContact(key, value);
        }
        super.onPause();
    }

    @Override
    public void onCountDelete(int productId) {
        dbAdapter.deleteContact(productId);
    }


    private void getProductInfo(Cart cart) {

        ProductItem apiService =
                CartApiClient.getClient().create(ProductItem.class);

        Call<Cart> call = apiService.FooResponse(deviceId,sessionId,cart);
        call.enqueue(new Callback<Cart>() {


            @Override
            public void onResponse(Call<Cart> call, Response<Cart> response) {
                if (response.isSuccessful()) {
                    Log.e("%&%&%&", "///" + response.message());
                }

            }

            @Override
            public void onFailure(Call<Cart> call, Throwable t) {
                Log.e("evan", "///" + t.getMessage());
            }
        });
    }
}