package com.example.evan.eShop.Spam;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by EVAN on 3/21/2017.
 */

public interface SampleInterface {
    String url = "http://192.168.1.58:8084/eshopservice/product";
   // String url = "http://192.168.0.3:8088/eshopservice/product";
    @GET("/list")
    public void Load(Callback<ArrayList<SampleAdapterModel>> response);
}
