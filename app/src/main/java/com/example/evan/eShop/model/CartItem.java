package com.example.evan.eShop.model;

import java.sql.Date;
import java.util.HashMap;

/**
 * Created by sbsatter on 3/18/17.
 */
public class CartItem {
    private Integer id, quantity;
    private Cart cart;
    private Date createdAt;
    private double price;
   private HashMap<String, Integer> product;


  //  private Product product;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

//    public Product getProduct() {
//        return product;
//    }
//
//    public void setProduct(Product  product) {
//        this.product = product;
//    }

    public Cart getCart() {

        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public HashMap<String, Integer> getProduct() {
        return product;
    }

    public void setProduct(HashMap<String, Integer> product) {
        this.product = product;
    }
}
