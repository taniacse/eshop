package com.example.evan.eShop.model;

/**
 * Created by sbsatter on 3/24/17.
 */
public class Color {
    private int id;
    private String name, code;

    public Color(int id, String color, String code) {
        this.id = id;
        this.name = color;
        this.code = code;
    }

    public Color() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
