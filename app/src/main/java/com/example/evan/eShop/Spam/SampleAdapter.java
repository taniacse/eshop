package com.example.evan.eShop.Spam;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.evan.eShop.interfaces.Listener;
import com.example.evan.eShop.R;

import java.util.ArrayList;

/**
 * Created by EVAN on 3/21/2017.
 */

public class SampleAdapter extends RecyclerView.Adapter<SampleAdapter.ViewHolder> {
    public ArrayList<SampleAdapterModel> android;

Listener listener;
    Context context;

    public SampleAdapter(Context mContext, ArrayList<SampleAdapterModel> android,Listener listener) {

        this.context = mContext;
        this.android = android;
        this.listener = listener;

    }

    @Override
    public SampleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout, parent, false);
        return new SampleAdapter.ViewHolder(view);
    }

    public void onBindViewHolder(SampleAdapter.ViewHolder holder, final int i) {

     holder.tv_name.setText(android.get(i).getProduct().getName());
    holder.tv_api_level.setText(android.get(i).getProduct().getCondition());
    holder.tv_version.setText(android.get(i).getProduct().getDescription());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.e("SampleAdapter", android.get(i).getId()+"==========="+i);
           listener.ShowDetails(android.get(i));
            }
        });


//        String image = android.get(i).getApplicantPicture();
//        String imageDataBytes = "";
//        if (image != null) {
//            if (image.length() > 0) {
//                imageDataBytes = image.substring(image.indexOf(",") + 1);
//                if (imageDataBytes.length() > 0) {
//                    Log.e("fg", "dfvbnd");
//                    if (IsBase64Encoded(imageDataBytes)) {
//                        holder.a.setImageBitmap(stringToBitmap(imageDataBytes));
//                    }
//                }
//            }
//        }

    }

    @Override
    public int getItemCount() {
        return android.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name, tv_version, tv_api_level, t;
        private ImageView a;

        public ViewHolder(View view) {
            super(view);
            tv_name = (TextView) view.findViewById(R.id.tv_name);
            tv_version = (TextView) view.findViewById(R.id.tv_version);
            tv_api_level = (TextView) view.findViewById(R.id.tv_api_level);

        }
    }

    public static Bitmap stringToBitmap(String fileString) {

        byte[] valueDecoded = Base64.decode(fileString, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(valueDecoded, 0,
                valueDecoded.length);
    }

    private boolean IsBase64Encoded(String value) {
        try {
            byte[] decodedString = Base64.decode(value, Base64.DEFAULT);
            BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}