package com.example.evan.eShop.sqliteDatabase;

/**
 * Created by EVAN on 4/2/2017.
 */

public class Product {

    private int quantity;
    private double unitPrice;
    private byte [] data;

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }



    private int id;
    private String name;
    private String code;

    public Product(int id, int quantity, String name, String code, String condition, String description,byte [] data) {
        this.quantity = quantity;
        this.id = id;
        this.name = name;
        this.code = code;
        this.condition = condition;
        this.description = description;
        this.data=data;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    private String condition;

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    //    private BigDecimal unitPrice;
//    private BigDecimal salePrice;1
//    private String condition;
    private String description;
}
