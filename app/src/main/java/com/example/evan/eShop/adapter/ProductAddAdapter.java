package com.example.evan.eShop.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.evan.eShop.Utils;
import com.example.evan.eShop.sqliteDatabase.ProductDatabase;
import com.example.evan.eShop.sqliteDatabase.Product;
import com.example.evan.eShop.R;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

/**
 * Created by EVAN on 4/6/2017.
 */

public class ProductAddAdapter extends RecyclerView.Adapter<ProductAddAdapter.ViewHolder> {

    public interface CountUpdate {
        void onCountUpdate(int productId, int count);
    }
    public interface CountDelete {
        void onCountDelete(int productId);
    }
    public ProductDatabase dbAdapter;
    private ArrayList<Product> mCartItems;
    private Context mContext;
    private CountUpdate mCountUpdate;
    private CountDelete mCountDelete;
    int count = 0;
    public ProductAddAdapter(Context context, CountUpdate countUpdate, CountDelete countDelete, ArrayList<Product> todoCursor) {
        mContext = context;
        mCartItems = todoCursor;
        mCountUpdate = countUpdate;
        mCountDelete = countDelete;
        }

    @Override
    public ProductAddAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layoutcard, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int i) {
        holder.tv_name.setText(mCartItems.get(i).getName());
        holder.tv_version.setText(mCartItems.get(i).getCondition());
        holder.tv_api_level.setText(mCartItems.get(i).getCode());

        byte[] bitmapdata = mCartItems.get(i).getData();

        Log.e(">>>>>",""+Utils.getImage(bitmapdata));
        holder.a.setImageBitmap(Utils.getImage(bitmapdata));

        holder.iteamamount.setText(String.valueOf(mCartItems.get(i).getQuantity()));
        holder.add_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count = Integer.parseInt(holder.iteamamount.getText().toString());
                count++;
                holder.iteamamount.setText(String.valueOf(count));
                mCountUpdate.onCountUpdate(mCartItems.get(i).getId(), count);
                //notifyDataSetChanged();
            }
        });
        holder.remove_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count = Integer.parseInt(holder.iteamamount.getText().toString());
                count--;
                if(count == 0){
                    mCountDelete.onCountDelete(mCartItems.get(i).getId());
                    mCartItems.remove(i);
                    notifyDataSetChanged();
                }
                holder.iteamamount.setText(String.valueOf(count));
                if(count>0)
                {
                    mCountUpdate.onCountUpdate(mCartItems.get(i).getId(), count);
                }
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(v.getContext(), String.valueOf(mCartItems.get(i).getName()), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mCartItems.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name, tv_version, tv_api_level, t, add_item, iteamamount, remove_item;
        private ImageView a;
        public ViewHolder(View view) {
            super(view);
            tv_name = (TextView) view.findViewById(R.id.item_name);
            tv_version = (TextView) view.findViewById(R.id.item_short_desc);
            tv_api_level = (TextView) view.findViewById(R.id.item_price);
            t = (TextView) view.findViewById(R.id.iteam_amount);
            add_item = (TextView) view.findViewById(R.id.add_item);
            iteamamount = (TextView) view.findViewById(R.id.iteam_amount);
            remove_item = (TextView) view.findViewById(R.id.remove_item);
            a = (ImageView) view.findViewById(R.id.product_thumb);
        }
    }

    public void product()
    {

    }
    public Bitmap ByteArrayToBitmap(byte[] byteArray)
    {
        ByteArrayInputStream arrayInputStream = new ByteArrayInputStream(byteArray);
        Bitmap bitmap = BitmapFactory.decodeStream(arrayInputStream);
        return bitmap;
    }
}
