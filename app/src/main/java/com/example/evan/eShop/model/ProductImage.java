package com.example.evan.eShop.model;

/**
 * Created by SayedMahmudRaihan on 3/17/2017.
 */
public class ProductImage {

    private Integer id;
    private ProductCommons productCommons;
    private Image image;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ProductCommons getProductCommons() {
        return productCommons;
    }

    public void setProductCommons(ProductCommons product) {
        this.productCommons = product;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }
}
