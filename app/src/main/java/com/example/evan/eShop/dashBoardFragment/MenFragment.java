package com.example.evan.eShop.dashBoardFragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;

import com.example.evan.eShop.CommonData;
import com.example.evan.eShop.main.MainActivity;
import com.example.evan.eShop.productToCartFragment.ProductFragment;
import com.example.evan.eShop.R;
import com.example.evan.eShop.adapter.MenExpandableListAdapter;
import com.example.evan.eShop.interfaces.categoryInteface;
import com.example.evan.eShop.model.Category;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;

public class MenFragment extends Fragment {
    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<Category> listDataHeader;
    HashMap<Integer, List<Category>> listDataChild;
    private String baseUrl;
    private String sessionId;
    private String deviceId;
ProgressDialog progress;
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View v = inflater.inflate(R.layout.fragment_men, container, false);
        CommonData commonData = (CommonData) getActivity().getApplication();

        baseUrl = commonData.getBaseUrl();
        MainActivity activitys = (MainActivity) getActivity();
        sessionId = activitys.getData();
        deviceId = activitys.getDeviceData();
        Log.e("deviceId", "VCVC" + deviceId);
        Log.e("sessionId", "VCVC" + sessionId);
        expListView = (ExpandableListView) v.findViewById(R.id.expandableListView1);
        listDataHeader = new ArrayList<Category>();
        listDataChild = new HashMap<Integer, List<Category>>();
        getdata();
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {

//                Log.e("Sv", "c" +
//                        listDataChild.get(
//                                listDataHeader.get(childPosition)).get(
//                                childPosition).getId());
                ProductFragment productFragment = new ProductFragment();
//                Bundle bundle = new Bundle();
//                bundle.putInt("id", groupId);
//                productFragment.setArguments(bundle);
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.app_bar_main, productFragment).addToBackStack(null);
                fragmentTransaction.commit();
                return false;
            }
        });
        return v;
    }
    private void getdata() {
        progress = new ProgressDialog(getContext());
        progress.setMessage("Data Loading ... Please Wait...");
        progress.show();

        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader("sessionId",sessionId);
               request.addHeader("deviceId", deviceId);

        }
        };
        final RestAdapter adapter1 = new RestAdapter.Builder().setEndpoint(baseUrl+categoryInteface.url).setRequestInterceptor(requestInterceptor).build();
        final categoryInteface restInterface = adapter1.create(categoryInteface.class);

        restInterface.Category(new Callback<ArrayList<Category>>() {
            @Override
            public void success(ArrayList<Category> categories, Response response) {
                if (categories != null && categories.size() > 0) {
                    int idx = 0;
                    for (Category category : categories) {

                        if (category.getParentCategoryId() == 0 ) {

                            if(category.getCategoryFor().equals("MEN") || category.getCategoryFor().equals("UNISEX")  )
                            {
                                listDataHeader.add(category);
                            }
                        } else {
                            if (!listDataChild.containsKey(category.getParentCategoryId())) {
                                listDataChild.put(category.getParentCategoryId(), new ArrayList<Category>());
                            }
                            if (listDataChild.containsKey(category.getParentCategoryId())) {
                                listDataChild.get(category.getParentCategoryId()).add(category);
                            }
                        }
                        idx++;
                    }
                }
                listAdapter = new MenExpandableListAdapter(getActivity(), listDataHeader, listDataChild);
                expListView.setAdapter(listAdapter);
                progress.dismiss();
            }
            public void failure(RetrofitError error) {
                Log.e("fg", "dfd" + error);
            }
        });
    }


}