package com.example.evan.eShop.model;


import com.example.evan.eShop.model.enums.PaymentStatus;

/**
 * Created by sbsatter on 3/21/17.
 */
public class Payment {
    private int id;
    private Enum<PaymentStatus> status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Enum<PaymentStatus> getStatus() {
        return status;
    }

    public void setStatus(Enum<PaymentStatus> status) {
        this.status = status;
    }



}
