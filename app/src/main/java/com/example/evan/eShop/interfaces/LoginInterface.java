package com.example.evan.eShop.interfaces;

import com.example.evan.eShop.model.UserCredentials;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.Header;

/**
 * Created by EVAN on 4/25/2017.
 */

public interface LoginInterface {
    String url = "auth";
    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST("/login")
    void Login( @Body UserCredentials ordObj, Callback<HashMap<String,String> >callback);

}
