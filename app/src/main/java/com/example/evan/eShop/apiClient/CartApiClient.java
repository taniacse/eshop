package com.example.evan.eShop.apiClient;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by EVAN on 4/16/2017.
 */

public class CartApiClient {

    public static final String BASE_URL = "http://192.168.1.244:8080/eshopservice/cart/";
    private static Retrofit retrofit = null;


    public static Retrofit getClient() {

        if (retrofit==null) {

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
