package com.example.evan.eShop.interfaces;

import com.example.evan.eShop.Spam.SampleAdapterModel;

/**
 * Created by EVAN on 3/22/2017.
 */

public interface Listener {
    void ShowDetails(SampleAdapterModel officeEmployeeModel);
}
